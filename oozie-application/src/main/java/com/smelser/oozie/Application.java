package com.smelser.oozie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;

/**
 * Created by paul.smelser@gmail.com on 03/10/15.
 * @author psmelser
 */
@SpringApplicationConfiguration
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
